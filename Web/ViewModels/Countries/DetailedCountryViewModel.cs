﻿namespace Web.ViewModels.Countries
{
    public class DetailedCountryViewModel : CountryViewModel
    {
        public string Capital { get; set; }
        public string CapitalLocalName { get; set; }
        public long Area { get; set; }
        public long Population { get; set; }

    }
}