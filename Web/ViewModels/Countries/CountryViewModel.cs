﻿namespace Web.ViewModels.Countries
{
    public class CountryViewModel : BaseViewModel
    {
        public string Name { get; set; }
        public string LocalName { get; set; }
    }
}