using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Web.ToExtract;
using Web.ToExtract.Finders;
using Web.ToExtract.Models;
using Web.ToExtract.Models.Countries;
using Web.ToExtract.Repositories;
using Web.ToExtract.Services;
using Web.ToExtract.Validators;
using Web.ViewModels;
using Web.ViewModels.Countries;

namespace Web
{
    internal sealed class DependienciesRoot
    {
        private readonly IServiceCollection _servicesCollection;
        private readonly IConfigurationRoot _configuration;

        public DependienciesRoot(
            IServiceCollection servicesCollection, 
            IConfigurationRoot configuration)
        {
            _servicesCollection = servicesCollection;
            _configuration = configuration;
        }

        public DependienciesRoot RegisterRepositories()
        {
            _servicesCollection.AddSingleton<IRepository<DetailedCountry>>(new XmlDetailedCountriesRepo(_configuration["XmlDataStorePath"]));
            return this;
        }

        public DependienciesRoot RegisterValidators()
        {
            _servicesCollection.AddTransient<IViewModelValidator<DetailedCountryViewModel>, DetailedCountryViewModelValidator>();
            return this;
        }

        public DependienciesRoot RegisterFinders()
        {
            _servicesCollection.AddSingleton<ICountriesFinder, CountriesFinder>();
            return this;
        }

        public DependienciesRoot RegisterServices()
        {
            _servicesCollection.AddSingleton<ICountriesService, CountriesService>();
            return this;
        }

    }
}