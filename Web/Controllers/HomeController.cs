﻿using Microsoft.AspNetCore.Mvc;
using Web.ToExtract;
using Web.ToExtract.Finders;
using Web.ToExtract.Services;
using Web.ViewModels;
using Web.ViewModels.Countries;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICountriesFinder _countriesFinder;
        private readonly ICountriesService _countriesService;

        public HomeController(
            ICountriesFinder countriesFinder,
            ICountriesService countriesService)
        {
            _countriesFinder = countriesFinder;
            _countriesService = countriesService;
        }
        public IActionResult Index() => View(_countriesFinder.GetAllDetailed());

        public IActionResult New(DetailedCountryViewModel model)
        {
            if (!ModelState.IsValid)
            {
                //TODO
                return RedirectToAction("Index");
            }
            _countriesService.AddCountry(model);
            return RedirectToAction("Index");
        }

        public IActionResult Error()
        {
            //TODO: Add Handling Exceptions
            return View();
        }
    }
}
