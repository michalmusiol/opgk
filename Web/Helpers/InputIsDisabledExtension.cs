﻿using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Web.Helpers
{
    [HtmlTargetElement("input", Attributes = ForAttributeName)]
    public class InputIsDisabledExtension : InputTagHelper
    {
        private const string ForAttributeName = "asp-for";

        [HtmlAttributeName("asp-is-disabled")]
        public bool IsDisabled { set; get; }

        public InputIsDisabledExtension(IHtmlGenerator generator) : base(generator)
        {
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (IsDisabled)
            {
                output.Attributes.Add(new TagHelperAttribute("disabled", "disabled"));
            }

            base.Process(context, output);
        }
    }
}
