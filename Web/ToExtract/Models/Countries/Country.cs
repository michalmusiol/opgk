﻿namespace Web.ToExtract.Models.Countries
{
    public class Country: BaseModel
    {
        public string Name { get; set; }
        public string LocalName { get; set; }
    }
}