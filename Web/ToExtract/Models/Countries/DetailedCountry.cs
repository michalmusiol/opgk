﻿namespace Web.ToExtract.Models.Countries
{
    public class DetailedCountry : Country
    {
        public string Capital { get; set; }
        public string CapitalLocalName { get; set; }
        public long Area { get; set; }
        public long Population { get; set; }
    }
}