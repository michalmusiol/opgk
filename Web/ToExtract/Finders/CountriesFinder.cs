﻿using System.Collections.Generic;
using Web.ToExtract.Mappers;
using Web.ToExtract.Models.Countries;
using Web.ToExtract.Repositories;
using Web.ViewModels.Countries;

namespace Web.ToExtract.Finders
{
    public class CountriesFinder : ICountriesFinder
    {
        private readonly IRepository<DetailedCountry> _repository;

        public CountriesFinder(IRepository<DetailedCountry> repository)
        {
            _repository = repository;
        }

        public IEnumerable<DetailedCountryViewModel> GetAllDetailed() => DetailedCountryViewModelMapper.Map(_repository.GetAll());
    }
}
