﻿using System.Collections.Generic;
using Web.ViewModels;
using Web.ViewModels.Countries;

namespace Web.ToExtract.Finders
{
    public interface ICountriesFinder
    {
        IEnumerable<DetailedCountryViewModel> GetAllDetailed();
    }
}