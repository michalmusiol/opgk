﻿using System.Text.RegularExpressions;

// ReSharper disable once CheckNamespace
namespace System
{
    public static class StringExtensions
    {
        public static string ReplaceSpecialChars(this string src, string replaceWith = "") => 
            Regex.Replace(src, "[^a-zA-Z0-9_.]+", replaceWith, RegexOptions.Compiled);
    }
}
