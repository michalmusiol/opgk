﻿// ReSharper disable once CheckNamespace
namespace System
{
    public static class StringHtmlExtensions
    {
        public static string ToListGroupToAnchorHref(this string src) =>
            string.Format($"#{src.ReplaceSpecialChars()}");

        public static string ToListGroupId(this string src) =>
            string.Format($"{src.ReplaceSpecialChars()}");
    }
}