﻿namespace Web.ToExtract.Validators
{
    public interface IValidator<in T>
    {
        void Validate(T model);
    }
}