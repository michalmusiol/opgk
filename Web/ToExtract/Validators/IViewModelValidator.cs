using Web.ViewModels;

namespace Web.ToExtract.Validators
{
    public interface IViewModelValidator<in T> : IValidator<T> where T : BaseViewModel
    {
    }
}