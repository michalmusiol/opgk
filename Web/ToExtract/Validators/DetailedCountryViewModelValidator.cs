﻿using System;
using System.Linq;
using Web.ToExtract.Models;
using Web.ToExtract.Models.Countries;
using Web.ToExtract.Repositories;
using Web.ViewModels;
using Web.ViewModels.Countries;

namespace Web.ToExtract.Validators
{
    public class DetailedCountryViewModelValidator: IViewModelValidator<DetailedCountryViewModel>
    {
        private readonly IRepository<DetailedCountry> _repository;

        public DetailedCountryViewModelValidator(IRepository<DetailedCountry> repository)
        {
            _repository = repository;
        }

        public void Validate(DetailedCountryViewModel model)
        {
            if (_repository.GetAll().Any(p => p.Name == model.Name))
            {
                throw new ArgumentOutOfRangeException(nameof(model.Name), "Name should be unique");
            }
        }
    }
}