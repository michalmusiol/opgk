﻿using Web.ToExtract.Models;

namespace Web.ToExtract.Validators
{
    public interface IModelValidator<in T>: IValidator<T> where T: BaseModel
    {
    }
}