﻿using System.Collections.Generic;
using Web.ToExtract.Models.Countries;

namespace Web.ToExtract.Repositories
{
    public class MockDetailedCountryRepo : IRepository<DetailedCountry>
    {
        private static readonly IList<DetailedCountry> Data = new List<DetailedCountry>()
        {
            new DetailedCountry()
            {
                Name = "Poland",
                LocalName = "Polska",
                Capital = "Warsaw",
                CapitalLocalName = "Warszawa",
                Area = 312679,
                Population = 38432992
            },
            new DetailedCountry()
            {
                Name = "Nederland",
                LocalName = "Nederland",
                Capital = "Amsterdam",
                CapitalLocalName = "Amsterdam",
                Area = 41526,
                Population = 17108799
            },
            new DetailedCountry()
            {
                Name = "Germany",
                LocalName = "Deutschland",
                Capital = "Berlin",
                CapitalLocalName = "Berlin",
                Area = 357376,
                Population = 80722792
            }
        };

        public IEnumerable<DetailedCountry> GetAll() => Data;

        public void AddAndCommit(DetailedCountry country) => Data.Add(country);
    }
}