﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Web.ToExtract.Models.Countries;

namespace Web.ToExtract.Repositories
{
    public class XmlDetailedCountriesRepo : IRepository<DetailedCountry>
    {
        public static Func<IEnumerable<DetailedCountry>> SeedDataFunc =  () => new MockDetailedCountryRepo().GetAll();

        public static bool OverrideIfCorrupted = true;

        public static bool Cache = true;

#pragma warning disable 169
        private IEnumerable<DetailedCountry> _prymitiveCache;
#pragma warning restore 169

        private readonly string _filePath;
        
        public XmlDetailedCountriesRepo(string filePath) => _filePath = filePath;

        public IEnumerable<DetailedCountry> GetAll()
        {
            if (!File.Exists(_filePath))
            {
                return SeedAndReturn();
            }

            try
            {
                if (Cache && _prymitiveCache != null)
                {
                    return _prymitiveCache;
                }

                return ReadXmlFile();
            }
            catch
            {
                if (OverrideIfCorrupted)
                {
                    return SeedAndReturn();
                }
                throw;
            }
        }

        public void AddAndCommit(DetailedCountry country)
        {
            var data = GetAll().ToList();
            RemoveXmlDataFile();
            data.Add(country);
            CreateXmlDataFile(data);
        }

        private static XmlSerializer CreateSerializer() => new XmlSerializer(typeof(List<DetailedCountry>));

        private IEnumerable<DetailedCountry> ReadXmlFile()
        {
            using (var reader = File.OpenRead(_filePath))
            {
                var data = (List<DetailedCountry>)CreateSerializer().Deserialize(reader);
                if (Cache)
                {
                    _prymitiveCache = data;
                }

                return data;
            }
        }

        private IEnumerable<DetailedCountry> SeedAndReturn()
        {
            // ReSharper disable PossibleMultipleEnumeration
            var data = SeedDataFunc.Invoke();
            CreateXmlDataFile(data);            

            return data;
        }

        private void CreateXmlDataFile(IEnumerable<DetailedCountry> data)
        {
            if (Cache)
            {
                _prymitiveCache = data;
            }

            using (var writer = File.CreateText(_filePath))
            {
                CreateSerializer().Serialize(writer, data);                
                writer.Flush();
            }
        }

        private void RemoveXmlDataFile()
        {
            if (File.Exists(_filePath))
            {
                File.Delete(_filePath);
            }
        }

    }
}