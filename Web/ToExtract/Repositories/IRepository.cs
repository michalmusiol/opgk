﻿using System.Collections.Generic;
using Web.ToExtract.Models;

namespace Web.ToExtract.Repositories
{
    public interface IRepository<T> where T : BaseModel
    {
        IEnumerable<T> GetAll();
        void AddAndCommit(T model);
    }
}