﻿using Web.ViewModels;
using Web.ViewModels.Countries;

namespace Web.ToExtract.Services
{
    public interface ICountriesService
    {
        void AddCountry(DetailedCountryViewModel country);
    }
}