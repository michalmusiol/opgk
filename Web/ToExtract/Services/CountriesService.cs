﻿using Web.ToExtract.Mappers;
using Web.ToExtract.Models.Countries;
using Web.ToExtract.Repositories;
using Web.ToExtract.Validators;
using Web.ViewModels.Countries;

namespace Web.ToExtract.Services
{
    public class CountriesService : ICountriesService
    {
        private readonly IRepository<DetailedCountry> _repository;
        private readonly IViewModelValidator<DetailedCountryViewModel> _viewModelValidator;


        public CountriesService(
            IRepository<DetailedCountry> repository, 
            IViewModelValidator<DetailedCountryViewModel> viewModelValidator)
        {
            _repository = repository;
            _viewModelValidator = viewModelValidator;
        }

        public void AddCountry(DetailedCountryViewModel country)
        {
            _viewModelValidator.Validate(country);
            _repository.AddAndCommit(DetailedCountryMapper.Map(country));
        }
    }
}