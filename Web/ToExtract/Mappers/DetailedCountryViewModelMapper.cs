﻿using System.Collections.Generic;
using System.Linq;
using Omu.ValueInjecter;
using Web.ToExtract.Models.Countries;
using Web.ViewModels.Countries;

namespace Web.ToExtract.Mappers
{
    public class DetailedCountryViewModelMapper
    {
        public static IEnumerable<DetailedCountryViewModel> Map(IEnumerable<DetailedCountry> data) => data.Select(Map).ToList();

        public static DetailedCountryViewModel Map(DetailedCountry data)
        {
            var viewModel = new DetailedCountryViewModel();
            viewModel.InjectFrom(data);
            return viewModel;
        }
    }
}