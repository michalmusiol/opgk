﻿using Omu.ValueInjecter;
using Web.ToExtract.Models.Countries;
using Web.ViewModels.Countries;

namespace Web.ToExtract.Mappers
{
    public static class DetailedCountryMapper
    {
        public static DetailedCountry Map(DetailedCountryViewModel data)
        {
            var model = new DetailedCountry();
            model.InjectFrom(data);
            return model;
        }
    }
}